
class Photographers {
  int id;
  String name;


  Photographers(
      {this.id,
        this.name,
        });

  factory Photographers.fromJson(Map<String, dynamic> json) {
    return Photographers(
        id : json['id'],
        name : json['name']
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;

    return data;
  }
}



