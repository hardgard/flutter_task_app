import 'package:flutter/material.dart';
import 'package:flutter_task_app/model/albums.dart';
import 'package:flutter_task_app/photos_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AlbumsList extends StatefulWidget {

  final int userId;

  AlbumsList({this.userId}):super();

  @override
  _AlbumsListState createState() => _AlbumsListState();/**/
}

class _AlbumsListState extends State<AlbumsList> {

  List<Albums> list = List();
  var isLoading = false;

  _fetchData(int id) async {
    setState(() {
      isLoading = true;
    });
    final response =
    await http.get("https://jsonplaceholder.typicode.com/albums/?userId=${id.toString()}");
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => Albums.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load albums list');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchData(widget.userId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Albums"),),
      body: isLoading? Center( child: CircularProgressIndicator(),):
      ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context,int index){
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Text(list[index].title),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>PhotosScreen(albumId: list[index].id,)));
                },
              ),
            );
          }),
    );
  }
}
