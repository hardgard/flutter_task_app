import 'package:flutter/material.dart';
import 'package:flutter_task_app/model/photos.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class PhotosScreen extends StatefulWidget {

  final int albumId;

  PhotosScreen({this.albumId}):super();

  @override
  _PhotosScreenState createState() => _PhotosScreenState();
}

class _PhotosScreenState extends State<PhotosScreen> {

  List<Photos> list = List();
  var isLoading = false;

  _fetchData(int id) async {
    setState(() {
      isLoading = true;
    });
    final response =
    await http.get("https://jsonplaceholder.typicode.com/photos?albumId=${id.toString()}");
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => Photos.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load albums list');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchData(widget.albumId);
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Albums"),),
      body: isLoading? Center( child: CircularProgressIndicator(),):
      ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context,int index){
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child:  Image.network(list[index].thumbnailUrl,fit: BoxFit.contain,width: 150,height: 150,),

            );
          }),
    );
  }
}
